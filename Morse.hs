module Morse where

import MorseLib

encode :: String -> [MorseUnit]
encode "A" = dit ++ dah ++ shortGap
encode "B" = dah ++ dit ++ dit ++ dit ++ shortGap
encode "C" = dah ++ dit ++ dah ++ dit ++ shortGap
encode "D" = dah ++ dit ++ dit ++ shortGap
encode "E" = dit ++ shortGap
encode "F" = dit ++ dit ++ dah ++ dit ++ shortGap
encode "G" = dah ++ dah ++ dit ++ shortGap
encode "H" = dit ++ dit ++ dit ++ dit ++ shortGap
encode "I" = dit ++ dit ++ shortGap
encode "J" = dit ++ dah ++ dah ++ dah ++ shortGap
encode "K" = dah ++ dit ++ dah ++ shortGap
encode "L" = dit ++ dah ++ dit ++ dit ++ shortGap
encode "M" = dah ++ dah ++ shortGap
encode "N" = dah ++ dit ++ shortGap
encode "O" = dah ++ dah ++ dah ++ shortGap
encode "P" = dit ++ dah ++ dah ++ dit ++ shortGap
encode "Q" = dah ++ dah ++ dit ++ dah ++ shortGap
encode "R" = dit ++ dah ++ dit ++ shortGap
encode "S" = dit ++ dit ++ dit ++ shortGap
encode "T" = dah ++ shortGap
encode "U" = dit ++ dit ++ dah ++ shortGap
encode "V" = dit ++ dit ++ dit ++ dah ++ shortGap
encode "W" = dit ++ dah ++ dah ++ shortGap
encode "X" = dah ++ dit ++ dit ++ dah ++ shortGap
encode "Y" = dah ++ dit ++ dah ++ dah ++ shortGap
encode "Z" = dah ++ dah ++ dit ++ dit ++ shortGap
encode "1" = dit ++ dah ++ dah ++ dah ++ dah ++ shortGap
encode "2" = dit ++ dit ++ dah ++ dah ++ dah ++ shortGap
encode "3" = dit ++ dit ++ dit ++ dah ++ dah ++ shortGap
encode "4" = dit ++ dit ++ dit ++ dit ++ dah ++ shortGap
encode "5" = dit ++ dit ++ dit ++ dit ++ dit ++ shortGap
encode "6" = dah ++ dit ++ dit ++ dit ++ dit ++ shortGap
encode "7" = dah ++ dah ++ dit ++ dit ++ dit ++ shortGap
encode "8" = dah ++ dah ++ dah ++ dit ++ dit ++ shortGap
encode "9" = dah ++ dah ++ dah ++ dah ++ dit ++ shortGap
encode "0" = dah ++ dah ++ dah ++ dah ++ dah ++ shortGap

codeWord :: String -> [MorseUnit]
codeWord xs = 
	if null xs then [] else (codeSymbol (head xs) ++ shortGap) ++ codeWord (tail xs)

codeText :: String -> [MorseUnit]
codeText xs = 
	if null (words xs) then [] else if length (words xs) == 1 then (codeWord (head (words xs))) ++ codeText (tail xs) else (codeWord (head (words xs)) ++ mediumGap) ++ codeText (tail xs)


decode :: [MorseUnit] -> String

decode x = if x == dit ++ dah then "A" 
	else if x == dah ++ dit ++ dit ++ dit then "B" 
	else if x == dah ++ dit ++ dah ++ dit then "C" 
	else if x == dah ++ dit ++ dit then "D" 
	else if x == dit then "E" 
	else if x == dit ++ dit ++ dah ++ dit then "F" 
	else if x == dah ++ dah ++ dit then "G" 
	else if x == dit ++ dit ++ dit ++ dit then "H" 
	else if x == dit ++ dit then "I" 
	else if x == dit ++ dah ++ dah ++ dah then "J" 
	else if x == dah ++ dit ++ dah then "K" 
	else if x == dit ++ dah ++ dit ++ dit then "L" 
	else if x == dah ++ dah then "M" 
	else if x == dah ++ dit then "N" 
	else if x == dah ++ dah ++ dah then "O" 
	else if x == dit ++ dah ++ dah ++ dit then "P" 
	else if x == dah ++ dah ++ dit ++ dah then "Q" 
	else if x == dit ++ dah ++ dit then "R" 
	else if x == dit ++ dit ++ dit then "S" 
	else if x == dah then "T" 
	else if x == dit ++ dit ++ dah then "U" 
	else if x == dit ++ dit ++ dit ++ dah then "V" 
	else if x == dit ++ dah ++ dah then "W" 
	else if x == dah ++ dit ++ dit ++ dah then "X" 
	else if x == dah ++ dit ++ dah ++ dah then "Y" 
	else if x == dah ++ dah ++ dit ++ dit then "Z" 
	else if x == dit ++ dah ++ dah ++ dah ++ dah then "1" 
	else if x == dit ++ dit ++ dah ++ dah ++ dah then "2" 
	else if x == dit ++ dit ++ dit ++ dah ++ dah then "3" 
	else if x == dit ++ dit ++ dit ++ dit ++ dah then "4" 
	else if x == dit ++ dit ++ dit ++ dit ++ dit then "5" 
	else if x == dah ++ dit ++ dit ++ dit ++ dit then "6" 
	else if x == dah ++ dah ++ dit ++ dit ++ dit then "7" 
	else if x == dah ++ dah ++ dah ++ dit ++ dit then "8" 
	else if x == dah ++ dah ++ dah ++ dah ++ dit then "9" 
	else if x == dah ++ dah ++ dah ++ dah ++ dah then "0" 
	else []



	 
